/**
 * Subscribe tests
 */

import * as subscribe from "../src/handlers/subscribe"
import uuid from 'uuid/v4';
import * as R from 'ramda'

describe("subscribe handler", ()=>{
    const dummyProjects = R.map(
        (int)=>{ 
            return ({
                id: `${int}`, 
                name: `proj-${int}`, 
                full_name: `Project ${int}`, 
                metadata:[]
            })
        },
        R.range(1,10),
    )


    it("creates a valid slack project picker", ()=>{
        const picker = subscribe.createProjectPicker(dummyProjects)
        expect(picker.type).toBe('select')
        expect(picker).toHaveProperty('name')
        expect(picker).toHaveProperty('label')
        expect(picker.options).toHaveLength(dummyProjects.length)
        picker.options.map((option)=>{
            expect(option).toHaveProperty('value');
            expect(option).toHaveProperty('label');
        })
    })

    it("handles slash command for subscribe", ()=>{
        // TODO: write this test
        expect(true).toBeFalsy()
    })
     
    it("handles create link form submission", ()=>{
        // TODO: write this test
        expect(true).toBeFalsy()
    })
})