/**
 * slack tests
 * @author KBjordahl
 */

import * as slack from "../src/services/slack";
import { WebClient } from "@slack/client";

jest.mock("@slack/client")

describe("slack", ()=>{
    const client = slack.getSlackFromEnv()

    beforeEach(()=>{
        WebClient.mockClear()
    })

    it("gets a slack client from env variables", ()=>{
        const client = slack.getSlackFromEnv();
        expect(WebClient).toHaveBeenCalledTimes(1);
    })

    it("gets the correct slack channel for slash commands", ()=>{
        const dummyReq = {
                channel_id: 1234,
                channel_name: "test"
            };

        const slackChannel = slack.getSlackChannel(dummyReq);
        expect(slackChannel.id).toBe(1234);
        expect(slackChannel.name).toBe("test")
    })

    it("gets the correct slack channel for actions", ()=>{
        const dummyReq = {
                channel: {
                    id: 1234,
                    name: "test"
                }
            };

        const slackChannel = slack.getSlackChannel(dummyReq);
        expect(slackChannel.id).toBe(1234);
        expect(slackChannel.name).toBe("test")
    })

    it("creates picker attachments", ()=>{
        const picker = slack.createPicker("test", "Test", [{label:"Option 1", value:"1"}])
        expect(picker).toMatchObject({
            label: "Test",
            name: "test",
            options: [{
                label: "Option 1",
                value: "1",
            }],
            type: "select"
        })
    })

    it("shows a dialog", ()=>{
        console.log(client)
        slack.showDialog(
            client,
            "1234",
            "7777",
            "Test Dialog",
            "Test Submit",
            []
        )
    })

})