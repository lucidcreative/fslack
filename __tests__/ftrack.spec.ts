/**
 * ftrack tests
 * @author KBjordahl
 */

import * as ftrack from "../src/services/ftrack"
import {Session} from "ftrack-node-api";
import * as R from 'ramda';

require("dotenv").config();
jest.mock("ftrack-node-api")


describe("ftrack", ()=>{
    // const session = new Session(        
    //         process.env.FTRACK_SERVER,
    //         process.env.FTRACK_API_USER,
    //         process.env.FTRACK_API_KEY,
    //     {autoConnectEventHub: false}
    //     );
    beforeEach(()=>{
        Session.mockClear()
    })    

    test("gets a session from environment", async ()=>{
        const testSession = ftrack.getFtrackFromEnv();
        expect(Session).toHaveBeenCalledTimes(1);
        // await testSession.initializing;
        // expect(testSession).toHaveProperty("serverInformation")
        // expect(testSession.initialized).toBeTruthy
    })

    describe("session actions", ()=>{

        // set up mock of session query
        const session = ftrack.getFtrackFromEnv();
        const resp = {data: [{id:1234, name:"test", full_name:"Test Project", metadata:[]}]};
        session.query.mockResolvedValue(resp);
        session.create.mockResolvedValue(resp);
        
        beforeEach(()=>{
            session.query.mockClear();
            session.create.mockClear();
        })

        test("gets a list of projects", async ()=>{
            
            // for now, we confirm we get at least one thing with an id
            const projects = await ftrack.getProjects(session)
            expect(session.query).toHaveBeenCalledTimes(1);
            expect(session.query).toHaveBeenCalledWith(
                `select name, full_name, id, metadata from Project where status='active'`
            )
            // R.map((testProject)=>{
            //     expect(testProject).toHaveProperty("__entity_type__")
            //     expect(testProject.__entity_type__).toBe("Project")
            //     expect(testProject).toHaveProperty("id")
            //     expect(testProject).toHaveProperty("name")
            //     expect(testProject).toHaveProperty("full_name")
            //     expect(testProject).toHaveProperty("metadata")
            // }, projects)
        })

        test("gets one project by id", async ()=>{
            const project = await ftrack.getProjectById(session, "1234");
            expect(session.query).toHaveBeenCalledTimes(1);
            expect(session.query).toHaveBeenCalledWith(
                `select name, full_name, id, metadata from Project where id='1234'`
            )
            // expect(project.full_name).toBe('fslack-dev');
            // expect(project.name).toBe('fslackdev');
            // expect(project.id).toBe("e8d041cc-684f-11e8-8e09-0a580a4c0921");
        })

        test("stores link on project as new metadata object", async ()=>{
            await ftrack.storeLinkOnFtrack(
                session,
                "1234",
                "7777",
                "test-channel"
            )
            expect(session.create).toHaveBeenCalledTimes(1)
            expect(session.create).toHaveBeenCalledWith(
                "Metadata", {
                    parent_id: "1234",
                    parent_type: "Project",
                    key: "link::test-channel",
                    value: "7777"
                }
            )
        })
                
    })

    test("generates link names correctly", ()=> {
        expect(ftrack.generateLinkName("test")).toBe("link::test")
    })

    test("generates link metadata correctly", ()=>{
        expect(ftrack.generateLinkMetadata(
            "12345",
            '7777',
            'test'
        )).toMatchObject({
            parent_type: 'Project',
            parent_id: "12345",
            key: "link::test",
            value: "7777"
        })
    })

    // test("stores link on ftrack project", ()=> {
    //     expect(ftrack.storeLinkOnFtrack(
    //         session, 
    //         "e8d041cc-684f-11e8-8e09-0a580a4c0921",
    //         'C8C0A4ACF',
    //         '9-franky_elbow'
    //     )).resolves.toBeTruthy();
    // })
})