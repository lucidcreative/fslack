ARG node_env=dev

FROM node:8

RUN mkdir /app

WORKDIR /app

COPY ./package*.json ./

ENV NODE_ENV=$node_env

RUN npm i