/**
 * Slack service functions
 * @author KBjordahl
 * @description functions to interact with slack
 */
import { WebClient } from "@slack/client"
import * as R from 'ramda'

export interface Pickable {
    label: string,
    value: string,
}

export interface Picker {
    type: string
    name: string
    label: string
    options: Array<Pickable>
}

export interface SlackChannel {
    id: string;
    name: string;
}

export function getSlackFromEnv(){
    const web = new WebClient(process.env.SLACK_TOKEN);
    return web;
}

export function getSlackChannel(payload: object):SlackChannel {
    return R.propOr({
        id: R.pathOr("", ["channel_id"], payload),
        name: R.pathOr("", ["channel_name"], payload)
    }, "channel", payload)
}

/**
 * Creates a picker object for sending to slack
 * @param name {string} internal name of the picker for form processing
 * @param label {string} the user-displayed label on the picker
 * @param list {Array<Pickable>} list of pickable items
 */
export const createPicker = R.curry(
    function createPicker(name:string, label:string, list:Array<Pickable>):Picker {
        return {label, name, options: list, type:"select"}
})

export function showDialog( slack:WebClient, trigger:string, 
    id:string, title:string, submit_label:string, elements:Array<object|Picker>):void{

    const dialog = {
        callback_id: `create-link::${id}`,
        title,
        submit_label,
        elements
    }

    slack.apiCall('dialog.open', {trigger_id: trigger, dialog:dialog})
}