/**
 * ftrack functions
 * @author KBjordahl
 * @description functions to handle interaction with ftrack
 */

import { Session } from 'ftrack-node-api';
import * as R from 'ramda';

export interface Project{
    id: string
    name: string
    full_name: string
    metadata: object
};

export interface Metadata {
    parent_id: string
    parent_type: string
    key: string
    value: string
};

export interface LinkEntity {
    entityId: string;
    entityType: string;
    parentId: string|null;
};

export interface UpdateEventData { 
    entities: Object[],
    pushToken: string,
    parents: string[]
    user: { 
        userid: string,
        name: string 
    };
    clientToken: string;
};

export interface UpdateEntity {
    keys: string[];
    entityType: string;
    objectTypeId: string;
    parents: LinkEntity[];
    parentId: string,
    action: string,
    entityId: string,
    changes: any;
};

/**
 * @description IMPURE! gets an ftrack session from env vars
 */
export function getFtrackFromEnv(autoConnectEventHub:boolean=false): Session {
    return new Session(        
        process.env.FTRACK_SERVER,
        process.env.FTRACK_API_USER,
        process.env.FTRACK_API_KEY,
    {autoConnectEventHub}
    )
}

export async function getProjects(session:Session):Promise<Array<Project>> {
    const projects = await session.query(
        `select name, full_name, id, metadata from Project where status='active'`
    )
    return projects.data
}

export async function getProjectById(session:Session, id: string): Promise<Project> {
    const project = await session.query(
        `select name, full_name, id, metadata from Project where id='${id}'`);
    return project.data[0];
}

export async function getEntityById(
    session:Session, id: string, type: string,
    fields:string[] = ['name', 'id']):Promise<object> {
    const entity = await session.query(
        `select ${fields.join(', ')} from ${getApiEntityType(type)} where id='${id}'`
    )
    return entity.data;
}

export async function storeLinkOnFtrack(
    session:Session, ftrackId: string, 
    slackId: string, slackName: string): Promise<Metadata> {
    const newMetadata = generateLinkMetadata(ftrackId, slackId,slackName);
    const result = await session.create("Metadata",newMetadata)
    return result.data
}

export function generateLinkName(name: string): string {
    return `link::${name}`;
}

export function generateLinkMetadata(ftrackId:string, slackId: string, slackName: string): Metadata{
    return {
        parent_id: ftrackId,
        parent_type: "Project",
        key: generateLinkName(slackName),
        value: slackId,
    }  
}

export function getApiEntityType(eventEntityType: string): string {
    switch ( eventEntityType ){
        case 'task':
            return "TypedContext";
        case 'show':
        case 'project':
            return "Project";
        case 'asset':
            return "Asset";
        case 'assetversion':
            return "AssetVersion";
        case 'version':
            return "Version";
        case 'note':
            return "Note";
    }
}