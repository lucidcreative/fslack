import { handleSlashSubscribe, handleCreateLinkSubmission } from "./handlers/subscribe";
import { registerFtrackListener } from './handlers/ftrackActivity';
import { getFtrackFromEnv } from "./services/ftrack";

// Import dependencies
const { createMessageAdapter } = require('@slack/interactive-messages');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');

const winston = require('winston'),
    expressWinston = require('express-winston');

// Create the adapter using the app's verification token, read from environment variable
const slackInteractions = createMessageAdapter(process.env.SLACK_VERIFICATION_TOKEN);
slackInteractions.action( /create-link::*./, handleCreateLinkSubmission)

// Initialize an Express application
// NOTE: You must use a body parser for the urlencoded format before attaching the adapter
const app = express();

expressWinston.requestWhitelist.push('body');
app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console({
        json: true,
        colorize: true,
        level: 'debug',
      })
    ]
  }));


app.use(bodyParser.urlencoded({ extended: false }));

// Attach the adapter to the Express application as a middleware
// NOTE: The path must match the Request URL and/or Options URL configured in Slack
app.use('/slack/actions', slackInteractions.expressMiddleware());
app.post('/slack/subscribe', (req, res) => {
    res.status(200).send('working on it...');
    winston.info(req.body);
    handleSlashSubscribe(req.body)
})
// Select a port for the server to listen on.
// NOTE: When using ngrok or localtunnel locally, choose the same port it was started with.
const port = process.env.PORT || 3000;

// Start the express application server
http.createServer(app).listen(port, () => {
  winston.info(`server listening on port ${port}`);
  registerFtrackListener(getFtrackFromEnv(true));
  winston.info("registered ftrack listeners");
});