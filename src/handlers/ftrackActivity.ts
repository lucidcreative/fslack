/**
 * ftrack activity handler
 */

import {Session, Event, EventHub} from 'ftrack-node-api';
import * as R from 'ramda';
import {} from "../services/slack"
import { UpdateEntity, UpdateEventData, getEntityById, getFtrackFromEnv } from "../services/ftrack"


export function registerFtrackListener(session:Session){
    session.eventHub.subscribe("topic=ftrack.update", handleUpdate)
}

export function handleUpdate(event:Event, ...args:any[]){
    console.log(args);
    const data = event.data;
    console.log(event)
    data.entities.map((ent)=>{console.log(ent)})
}

export function formatUpdatePost(session:Session, updateEvent:UpdateEventData):any {
    
}

export async function formatUpdateAttachment(session:Session, updateEntity:UpdateEntity){
    const entity = await getEntityById(session, updateEntity.entityId, updateEntity.entityType, ['id', 'name', 'link']) 
    return {
        title: entity.name,
    }
}
// export class FslackMonitor {

//     identifier= "fslack-activity-monitor";
//     version= '180608a';
//     session:Session;

//     constructor (session:Session) {
//         this.session = session;
//     }

//     register = ()=>{
//         winston.info(`Subscribing ${this.identifier}`)
//         // this.session.eventHub.subscribe("topic=ftrack.action.discover", this.discover);
//         // this.session.eventHub.subscribe("topic=ftrack.action.launch", this.launch);
//         // this.session.eventHub.subscribe("topic=ftrack.update", this.handleUpdateEvent);
//         this.session.eventHub.subscribe("topic=ftrack.update", this.log)
//     }

//     log = (event:Event)=>{
//         console.log(event);
//         event.data.entities.map((entity)=>{console.log(entity)})
//     }
    
// }
// let session = new Session(
//     process.env.FTRACK_SERVER,
//     process.env.FTRACK_API_USER,
//     process.env.FTRACK_API_KEY,
//     {autoConnectEventHub: true}
//     );

// export const monitor = new FslackMonitor(session);
