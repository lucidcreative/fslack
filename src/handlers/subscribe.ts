/**
 * Subscribe handler functions
 * @author KBjordahl
 * @description handles subscriptions from the slack slash command
 */

import * as R from 'ramda';
import uuid from 'uuid/v4';
import { renameKeys } from '../util'
import { getFtrackFromEnv, getProjects, Project, storeLinkOnFtrack, getProjectById } from '../services/ftrack';
import { createPicker, showDialog, getSlackFromEnv, getSlackChannel, SlackChannel } from '../services/slack';
/**
 * gets the selected project from a submitted slack form
 * @param payload {object} the payload delivered from slack (not the body!)
 */
export function getSelectedFtrackProjectId(payload: object):string {
    return R.pathOr("", ["submission","project"], payload);
}

export function createProjectPicker(projects: Array<Project>){

    return R.compose(
        createPicker("project", "ftrack Project"),
        R.map(
            (project:Project)=>{return({value: project.id, label: project.full_name})},
        )
    )(projects)
}

/**
 * /**
 * IMPURE! Triggers dialog to ask user for project from ftrack
 * @param payload slack slash command payload
 */

export async function handleSlashSubscribe(payload: object): Promise<void> {
    const session = getFtrackFromEnv()
    const picker = createProjectPicker( await getProjects(session) )
    const trigger = R.pathOr("",['trigger_id'], payload);
    const callback_id = uuid();
    showDialog(
        getSlackFromEnv(),
        trigger,
        callback_id,
        "Link ftrack Project",
        "Link",
        [picker]
    )
}


export async function handleCreateLinkSubmission(payload:object, respond:any): Promise<void> {
    const channel = getSlackChannel(payload);
    const session = getFtrackFromEnv()
    const ftrackId = getSelectedFtrackProjectId(payload);
    try{
        const result = await storeLinkOnFtrack(
            session,
            ftrackId,
            channel.id,
            channel.name
        )
        const project = await getProjectById(session, result.parent_id)
        respond({text: `_ successfully linked this channel to :ftrack-service: ${project.full_name} _`})
    } catch {
        respond({text:"_*  Looks like we hit a snag... *_"})
    }
}